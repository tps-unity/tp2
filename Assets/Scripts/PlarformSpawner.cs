using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class PlarformSpawner : MonoBehaviour
{
    public float PlatformsY = -0.5f;
    public float MinDistance = 1f;
    public float MaxDistance = 3f;
    public float MinWidth = 1f;
    public float MaxWidth = 5f;
    public float MinCoinHeight = 2f;
    public float MaxCoinHeight = 4f;
    public int NumActivePlatform = 5;
    public float Coin1SpawnRate = 0.4f;
    
    [SerializeField]
    private PlatformScript startPlatform;
    [SerializeField]
    private GameObject platformPrefab;
    [SerializeField]
    private GameObject coinPrefab;
    private List<PlatformScript> platforms = new();

    void Start()
    {
        startPlatform.Destroyed.AddListener(PlatformDestroyed);
        platforms.Add(startPlatform);
    }

    void Update()
    {
        while (platforms.Count < NumActivePlatform)
        {
            SpawnPlatform();
        }
    }

    void SpawnPlatform()
    {
        PlatformScript previous = platforms.Last();
        platforms.Add(Instantiate(platformPrefab).GetComponent<PlatformScript>());

        float platformWidth = Random.Range(MinWidth, MaxWidth);

        PlatformScript platform = platforms.Last();
        platform.transform.localScale = new Vector2(platformWidth, 1f);
        platform.transform.position = new Vector2(previous.GetRightX() + (platformWidth / 2f) + Random.Range(MinDistance, MaxDistance), PlatformsY);
        platform.Destroyed.AddListener(PlatformDestroyed);

        if (Random.Range(0f, 1f) <= Coin1SpawnRate)
        {
            Coin coin = Instantiate(coinPrefab).GetComponent<Coin>();
            coin.Value = 1;
            coin.transform.position = platform.transform.position + (Vector3.up * Random.Range(MinCoinHeight, MaxCoinHeight)) + (Vector3.right * Random.Range(-platformWidth / 2f, platformWidth / 2f));
            coin.transform.localScale = new Vector3(0.5f, 0.5f, 0.1f);
            coin.transform.SetParent(platform.transform, true);
            
        }
    }

    void PlatformDestroyed(PlatformScript platform)
    {
        platforms.Remove(platform);
    }
}
