using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;



public class Edge : MonoBehaviour
{
    public GameObject GameOver;
    
    private GameConfig config;

    void Start()
    {
        config = GameObject.Find("GameConfig").GetComponent<GameConfig>();
        GameOver.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            config.CurrentSpeed = config.DefaultSpeed;

            GameOver.SetActive(true);
            PauseGame();
        }
    }
    void PauseGame()
    {
        Time.timeScale = 0f;
    }
}
