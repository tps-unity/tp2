using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIController : MonoBehaviour
{
    private TMP_Text scoreValue, coinValue;
    private PlayerScript player;

    void Start()
    {
        scoreValue = GameObject.Find("ScoreValue").GetComponent<TMP_Text>();
        coinValue = GameObject.Find("CoinValue").GetComponent<TMP_Text>();
        player = GameObject.Find("Player").GetComponent<PlayerScript>();
    }

    void Update()
    {
        scoreValue.text = player.Score.ToString();
        coinValue.text = player.Coins.ToString();
    }
}
