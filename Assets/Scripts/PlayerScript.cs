using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    [SerializeField]
    Rigidbody2D rb;

    private bool grounded = true, canDoubleJump = true;
    private int score = 0, coins = 0;
    private GameConfig config;

    public float jumpForce = 5f;
    public int Score => score;
    public int Coins => coins;

    void Start()
    {
        config = GameObject.Find("GameConfig").GetComponent<GameConfig>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && (grounded || canDoubleJump))
        {
            float force = jumpForce;

            if (!grounded && canDoubleJump)
            {
                canDoubleJump = false;
                force *= 0.8f;
                rb.velocity = Vector2.zero;
            }
            else
            {
                canDoubleJump = true;
            }

            rb.AddForce(Vector2.up * force, ForceMode2D.Impulse);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Platform"))
        {
            grounded = true;
            canDoubleJump =  false;

            PlatformScript platform = other.gameObject.GetComponent<PlatformScript>();

            if (!platform.wasHit)
            {
                platform.wasHit = true;
                AddScore(1);
            }
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Platform"))
        {
            grounded = false;
        }
    }

    public void AddScore(int amount)
    {
        score += amount;

        config.CurrentSpeed += config.SpeedIncrement;
    }

    public void AddCoins(int amount)
    {
        coins += amount;
    }
}
