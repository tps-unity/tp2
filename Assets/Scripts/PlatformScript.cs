using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlatformScript : MonoBehaviour
{
    public float DestroyX = -11f;
    public UnityEvent<PlatformScript> Destroyed = new UnityEvent<PlatformScript>();
    public bool wasHit = false;

    private bool moving = true;
    private GameConfig config;

    void Start()
    {
        config = GameObject.Find("GameConfig").GetComponent<GameConfig>();
    }

    void Update()
    {
        if (moving)
        {
            transform.Translate(Vector2.left * config.CurrentSpeed * Time.deltaTime);
        }

        if (GetRightX() <= DestroyX)
        {
            config.CurrentSpeed += config.SpeedIncrement;
            Destroy(gameObject);
            Destroyed?.Invoke(this);
        }
    }

    public float GetRightX()
    {
        return transform.position.x + (transform.localScale.x / 2f);
    }
}
