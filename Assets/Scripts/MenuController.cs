using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public CanvasGroup OptionPanel;

    public GameObject GameOver;

    void Update()
    {
        if (GameOver.activeSelf && Input.GetKeyUp(KeyCode.Space))
        {
            RestartGame();
        }
    }

    public void PlayGame()
    {   
        ResumeGame();
        SceneManager.LoadScene(1);
    }

    public void Option()
    {
        OptionPanel.alpha = 1;
        OptionPanel.blocksRaycasts = true;
    }

    public void Back()
    {
        OptionPanel.alpha = 0;
        OptionPanel.blocksRaycasts = false;
    }

    public void QuitGame()
    {
       SceneManager.LoadScene(0);
    }

    public void RestartGame()
    {  
       ResumeGame();
       SceneManager.LoadScene(1);
    }

     void ResumeGame()
    {
        Time.timeScale = 1f;
    }

}
