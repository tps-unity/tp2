using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConfig : MonoBehaviour
{
    public float DefaultSpeed = 5f;
    public float CurrentSpeed = 5f;
    public float SpeedIncrement = 0.01f;
}
